class DateRange(val start: MyDate, val end: MyDate): Iterable<MyDate>{
    override fun iterator():Iterator<MyDate>{
        return object: Iterator<MyDate>{
            var current: MyDate = start
            var result: MyDate = start

            override fun hasNext(): Boolean{
                if (current <= end) return true
                else return false
            }

            override fun next(): MyDate {
                if (hasNext() == true){
                    result = current
                    current = current.followingDate()
                    return result
                }
                else throw NoSuchElementException()
            }
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)
// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate {
    return addTimeIntervals(timeInterval, 1)
}

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

data class IntervalsMultiplied(val timeInterval: TimeInterval, val multiplier: Int)

operator fun TimeInterval.times(multiplier: Int): IntervalsMultiplied{
    return IntervalsMultiplied(this, multiplier)
}

operator fun MyDate.plus(timeInterval: IntervalsMultiplied): MyDate {
    return addTimeIntervals(timeInterval.timeInterval, timeInterval.multiplier)
}


fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}